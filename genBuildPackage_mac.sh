#!/bin/sh

mkdir build/assets
mv build/static/css/*.css build/assets
mv build/static/js/*.js build/assets
mv build/static/media/* build/assets
rm -rf build/static
mv build/* build/assets
cp -r zendesk-mock/assets/* build/assets
rm build/assets/iframe.html
rm build/assets/favicon.ico
cp -r zendesk-mock/translations build
cp zendesk-mock/manifest.json build
perl -pe "s/http\:\/\/localhost\:[0-9]+/assets\/index.html/g" build/manifest.json > ./build/manifest2.json && mv ./build/manifest2.json ./build/manifest.json
perl -pe "s/.\/static\/[^\/]+\///g" build/assets/asset-manifest.json > ./build/assets/asset-manifest2.json && mv ./build/assets/asset-manifest2.json ./build/assets/asset-manifest.json
perl -pe "s/.\/static\/[^\/]+\///g" build/assets/index.html > ./build/assets/index2.html && mv ./build/assets/index2.html ./build/assets/index.html
# perl -pe "s/.\/static\/[^\/]+\///g" build/assets/precache-manifest > ./build/assets/precache-manifest2 && mv ./build/assets/precache-manifest2 ./build/assets/precache-manifest
perl -pe "s/url.\//url(/g" build/assets/main.*.chunk.css > ./build/assets/main.*.chunk2.css && mv ./build/assets/main.*.chunk2.css ./build/assets/main.*.chunk.css

cd  build
# zat package
zcli apps:package
