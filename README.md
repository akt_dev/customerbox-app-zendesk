## CustomerBox Serverless


## How to install

1- `npm i`

## Pre-configure

1- Install husky `npm run prepare`

2- Override file located in `.husky/_/husky.sh` to `docs/husky.sh`

## How to run

1- On main folder run: `npm run start`
2- on zendes-mock folder run: `zcli apps:server`


## Build files

`build/tmp/`

## Change Log


### v1.0.0 - 10/05/2021
1- A lot of fixes

### Hall of Fame

PO: Arthur Meyer (ameyer@2listen.com.br)

Scrum Master: Jackeline Saez (jsaez@aktienow.com)

Dev: Leandro Simões (lsimoes@aktienow.com)
Dev: Robert Santos (rsantos@aktienow.com)

Ex-PO: Fernando Casanova (fcasanova@aktienow.com)
