import React from 'react';
import { render, fireEvent, act } from '@testing-library/react';
import Order from './Order';
import { mock } from './Order.mock';

jest.mock('../../../Services/Zendesk', () => ({
  getSettings: () => [jest.fn()],
  modal: () => new Promise((res) => res(1)),
  modalListItens: () => new Promise((res) => res(1)),
  size: (width, height) => null,
  resize: (width, height) => null,
  client: {
    get: () => new Promise((res) => res(1)),
    on: () => new Promise((res) => res(1)),
  },
  scrollRef: {
    current: {
      scrollIntoView: () => {}
    }
  }
}));

describe('Order Component', () => {
  it('Renders without crashing', () => {
    render(<Order />);
  });

  it('props work', () => {
    const {
      updated,
      orderData,
    } = mock;
    const {
      getByText,
    } = render(<Order updated={updated} orderData={orderData} />);

    expect(getByText(`${orderData.deliveryId}`)).toBeInTheDocument();
    expect(getByText(`${updated}`)).toBeInTheDocument();
    expect(getByText(`${orderData.customerName}`)).toBeInTheDocument();
    expect(getByText(`${orderData.codeOrder}`)).toBeInTheDocument();
    expect(getByText(`${orderData.shipMethod}`)).toBeInTheDocument();
    expect(getByText(`${orderData.shipCode}`)).toBeInTheDocument();
    expect(getByText(`${orderData.status}`)).toBeInTheDocument();
    expect(getByText(`${orderData.shipCode}`)).toHaveAttribute('href', orderData.trackingUrl);
  });

  it('Sum total value of items', () => {
    const {
      updated,
      orderData,
    } = mock;
    const {
      getByText,
    } = render(<Order updated={updated} orderData={orderData} />);

    expect(getByText('Valor total: R$4.20')).toBeInTheDocument();
  });

  it('Check if the list-button exist', async () => {
    // jest.spyOn(jest.fn, 'modal').mockImplementation(() => true);
    const {
      updated,
      orderData,
    } = mock;
    const {
      getByText, getByTestId
    } = render(<Order updated={updated} orderData={orderData} />);

    await act(async () => {
      // Simula evento do click no botão com esse texto
      fireEvent.click(getByTestId('list-button'));
    });

    expect(getByTestId('list-button')).toBeEnabled();
  });
});
