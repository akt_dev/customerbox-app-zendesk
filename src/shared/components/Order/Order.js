/* eslint-disable no-restricted-syntax */
/* eslint-disable react/prop-types */
import React, { useEffect, useState } from 'react';
import {
  Typography,
  Grid,
} from '@material-ui/core';

import { VscRocket } from 'react-icons/vsc';
import { AiOutlineShoppingCart } from 'react-icons/ai';
import { FaExpandAlt } from 'react-icons/fa';
import { MdAttachMoney } from 'react-icons/md';

import { makeStyles } from '@material-ui/core/styles';
import Progress from '../Progress/Progress';

import Zaf from '../../../Services/Zendesk';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
  },
  gridMargin: {
    marginBottom: theme.spacing(1),
  },
  primaryText: {
    color: '#993fca',
    padding: '0 5px 0 0',
  },
  secondaryText: {
    position: 'relative',
    top: '1px',
  },
  items: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
    width: '97%',
  },
  product: {
    width: '100%',
    backgroundColor: '#F8E4E7',
    padding: '10px',
    borderRadius: '10px',
    marginBottom: '10px',
  },
  buttonPrimary: {
    color: '#fff',
    background: '#993fca',
    borderColor: '#993fca',
    '&:hover': {
      background: '#bea3ce',
      color: '#993fcc',
    },
  },
  buttonSecondary: {
    color: '#993fca',
    borderColor: '#993fca',
    '&:hover': {
      background: '#bea3ce',
      color: '#fff',
    },
  },
  deliverContainer: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
    flexDirection: 'row',
  },
  b2wLogoContainer: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
    flexDirection: 'row',
    padding: '5px 5px 5px 0',
  },
  icon: {
    width: '20px',
    height: '20px',
    position: 'relative',
    top: '4px',
    padding: '0 5px 0 0px',
  },
  logo: {
    color: '#06E7D2',
    fontWeight: 'bold',

  },
  separator: {
    borderBottom: '1px solid #ccc',
    padding: '3px',
    width: '95%',
  },
  itensHeaderContainer: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: '5px 0 7px 0',
  },
  listProducts: {
    color: '#7D807E',
    fontSize: '13.7px',
  },
}));

const Order = ({ updated, orderData, listItens = '' }) => {
  const classes = useStyles();
  const [infoOrder, setInfoOrder] = useState([]);
  const [totalOrder, setTotalOrder] = useState(0);
  const [instance, setInstance] = useState('');
  const { client } = Zaf;

  const handleOpenModalItem = () => {
    Zaf.modalListItens('list', instance);
  };

  useEffect(() => {
    Zaf.client.get('instances').then((instancesData) => {
      const { instances } = instancesData;
      for (const instanceGuid in instances) {
        if (
          instances[instanceGuid].location === 'ticket_sidebar'
          || instances[instanceGuid].location === 'new_ticket_sidebar'
        ) {
          setInstance(client.instance(instanceGuid));
        }
      }
    });
    if (orderData && orderData.items) {
      setInfoOrder([
        { id: 0, label: 'Cliente', info: orderData.customerName },
        { id: 1, label: 'Pedido', info: orderData.codeOrder },
        { id: 2, label: 'Tipo de transporte', info: orderData.shipMethod },
        {
          id: 3,
          label: 'Código de transporte',
          info: orderData.shipCode,
          link: orderData.trackingUrl,
        },
        { id: 4, label: 'Status atual', info: orderData.status },
        { id: 5, label: 'Última atualização', info: updated },
      ]);

      // console.log(orderData.items)
      const total = JSON.parse(orderData.items).reduce((acc, curr) => acc + curr.original_price, 0);
      setTotalOrder(total);
    }
  }, []);

  return (
    <>
      {infoOrder && infoOrder.length > 0
        ? (
          <div className={classes.root}>
            <Grid item className={classes.gridMargin}>
              <div className={classes.deliverContainer}>
                <Typography
                  className={classes.primaryText}
                  variant="body1"
                >
                  <VscRocket className={classes.icon} />
                  Entrega
                </Typography>
                <Typography
                  className={classes.secondaryText}
                  variant="body1"
                >
                  {orderData.deliveryId}
                </Typography>
              </div>
              <div className={classes.b2wLogoContainer}>
                <Typography
                  className={classes.logo}
                  variant="body1"
                >
                  B2W
                </Typography>
                <Typography
                  variant="body1"
                >
                  MARKETPLACE
                </Typography>
              </div>
              {
                infoOrder.map((item) => (
                  <Typography key={item.id} variant="body2" style={{ paddingTop: 3 }}>
                    <span style={{ fontWeight: 'bolder' }}>{item.label}</span>
                    :
                    {' '}
                    {item.link
                      ? (
                        <span>
                          {' '}
                          <a href={item.link} target="_blank" rel="noreferrer">
                            {' '}
                            {item.info}
                            {' '}
                          </a>
                          {' '}
                        </span>
                      )
                      : (<span>{item.info}</span>)}
                  </Typography>

                ))
              }
              <div className={classes.separator} />
            </Grid>
            <Grid item className={classes.gridMargin}>
              <div className={classes.itensHeaderContainer}>
                <Typography className={classes.primaryText} variant="body1">
                  <AiOutlineShoppingCart className={classes.icon} />
                  Itens
                  {' '}
                </Typography>
                <Typography data-testid="list-button" onClick={listItens || handleOpenModalItem} className={classes.listProducts} style={{ cursor: 'pointer' }} variant="body1">
                  Ver lista de produtos
                  {' '}
                  <FaExpandAlt className={classes.icon} />
                </Typography>
              </div>
              <div style={{ width: '100%', overflow: 'auto', height: '25vh' }}>
                <Grid container className={classes.items}>
                  {orderData && orderData.items
                    ? JSON.parse(orderData.items).map((item) => (
                      <div className={classes.product} key={Date.now()}>
                        <Typography style={{ marginBottom: '10px' }} variant="body2">
                          <b>Produto:</b>
                          {item.name}
                        </Typography>
                        <Typography style={{ marginBottom: '10px' }} variant="body2">
                          <b>ID B2W:</b>
                          {item.product_id}
                        </Typography>
                        <Typography style={{ marginBottom: '10px' }} variant="body2">
                          <b>Quantidade:</b>
                          {item.qty}
                        </Typography>
                        <Grid item xs={12}>
                          <Typography variant="body2">
                            <b>Valor unitário:</b>
                            {`R$ ${Number(item.original_price).toFixed(2)}.`}
                          </Typography>
                        </Grid>
                      </div>
                    ))
                    : ''}
                </Grid>
              </div>
              <Typography
                style={{ paddingTop: 5 }}
                className={classes.primaryText}
                variant="body1"
              >
                <MdAttachMoney className={classes.icon} />
                {`Valor total: R$${Number(totalOrder).toFixed(2)}`}
              </Typography>
            </Grid>
          </div>
        )
        : (<Progress />)}
    </>
  );
};

export default Order;
