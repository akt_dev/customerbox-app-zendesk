import React from 'react';
import { render } from '@testing-library/react';
import { mock } from './Products.mock';
import Products from './Products';

describe('Order Component', () => {
  it('Renders without crashing', () => {
    render(<Products itens={mock.orderData.items}/>);
  });
});
