/* eslint-disable react/no-array-index-key */
/* eslint-disable react/prop-types */
import React from 'react';

import {
  Typography,
  Grid,
} from '@material-ui/core';

import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() => ({
  items: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    flexWrap: 'wrap',
    height: '10px',
  },
  product: {
    overflow: 'auto',
    lineBreak: 'anywhere',
    width: '100%',
    maxWidth: '300px',
    height: '140px',
    backgroundColor: '#F8E4E7',
    padding: '10px',
    borderRadius: '10px',
    marginBottom: '10px',
    marginLeft: '10px',
  },
}));

const Product = ({ itens }) => {
  const classes = useStyles();

  return (
    <Grid data-testid="product_grid" container className={classes.items}>
      {itens.map((item, index) => (
        <div className={classes.product} key={index}>
          <Typography style={{ marginBottom: '10px' }} variant="body2">
            <b>Produto:</b>
            {item.name}
          </Typography>
          <Typography style={{ marginBottom: '10px' }} variant="body2">
            <b>ID B2W:</b>
            {item.product_id}
          </Typography>
          <Typography style={{ marginBottom: '10px' }} variant="body2">
            <b>Quantidade:</b>
            {item.qty}
          </Typography>
          <Grid item xs={12}>
            <Typography variant="body2">
              <b>Valor unitário:</b>
              {`R$ ${Number(item.original_price).toFixed(2)}.`}
            </Typography>
          </Grid>
        </div>
      ))}
    </Grid>
  );
};

export default Product;
