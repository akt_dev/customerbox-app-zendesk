import React from 'react';
import {
  CircularProgress,
} from '@material-ui/core';

const Progress = () => (
  <div style={{
    display: 'flex', flexDirection: 'row', justifyContent: 'center', alignItems: 'center',
  }}
  >
    <CircularProgress style={{ color: '#993fca' }} />
  </div>
);

export default Progress;
