/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable react/prop-types */
import React from 'react';
import {
  Button,
} from '@material-ui/core';

import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() => ({
  buttonPrimary: {
    textTransform: 'none',
    color: '#fff',
    width: '95%',
    height: '40px',
    fontSize: '13px',
    fontWeight: 'bold',
    background: '#FA971F',
    borderColor: '#FA971F',
    '&:hover': {
      background: '#FBC27D',
      color: '#FA971F',
    },
  },
  buttonSecondary: {
    textTransform: 'none',
    color: '#FA971F',
    width: '95%',
    height: '40px',
    borderColor: '#FA971F',
    fontWeight: 'bold',
    fontSize: '13px',
    '&:hover': {
      background: '#FBC27D',
      color: '#fff',
    },
  },
}));

const CustomButton = ({ title, size = 'small', ...otherProps }) => {
  const classes = useStyles();
  let classeButton = classes.buttonPrimary;
  if (otherProps.secondary) {
    classeButton = classes.buttonSecondary;
    // eslint-disable-next-line no-param-reassign
    delete otherProps.secondary;
  }

  return (
    <Button
      size={size}
      variant="outlined"
      className={classeButton}
      {...otherProps}
    >
      {title}
    </Button>
  );
};

export default CustomButton;
