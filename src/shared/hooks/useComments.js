import { useEffect, useState } from 'react';
import Zaf from '../../Services/Zendesk';

const useComments = () => {
  const [updatedAt, setUpdatedAt] = useState('2020-01-01');
  const [updateComments, setUpdateComments] = useState(0);

  // GET TICKET AND COMMENTS
  useEffect(() => {
    Zaf.client.get('ticket')
      .then(async (data) => {
        const {
          tags,
        } = data.ticket;
        const lastUpdated = tags
          .find((elem) => elem.indexOf('last_message:') >= 0);
        if (lastUpdated) {
          setUpdatedAt(() => lastUpdated.split('last_message:')[1]);
        }
      })
      .catch((err) => err);
  }, [updateComments]);

  return [updatedAt, setUpdateComments];
};
export default useComments;
