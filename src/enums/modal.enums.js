const EChats = Object.freeze({
  NORMAL: 'CUSTOMER_B2W',
  SELLER: 'B2W_SELLER',
  CUSTOMER_SELLER: 'CUSTOMER_SELLER',
});

const ModalSize = Object.freeze({
  WIDTH: '170vh',
  HEIGHT: '80vh',
});

const ETypesB2W = Object.freeze({
  normal: 'CUSTOMER_B2W',
  b2w: 'B2W_SELLER',
  customSeller: 'CUSTOMER_SELLER',
});

const ETypes = Object.freeze({
  NORMAL: 'normal',
  SELLER: 'b2w',
  CUSTOMER_SELLER: 'customSeller',
});

const EChatStatus = Object.freeze({
  ACTIVE: 'ativo',
  PAUSED: 'pausado',
  ARCHIVED: 'arquivado',
});

const EChatStatusB2W = Object.freeze({
  ACTIVE: 'ACTIVE',
  PAUSED: 'PAUSED',
  ARCHIVED: 'ARCHIVED',
});

const EChatStatusClasses = Object.freeze({
  ATIVO: 'info',
  PAUSADO: 'warning',
  ARQUIVADO: 'error',
});

const EMARKETPLACE = Object.freeze({
  B2W: 'b2w',
  MAGALU: 'magalu',
});

export {
  EChats,
  ModalSize,
  ETypes,
  ETypesB2W,
  EChatStatus,
  EChatStatusClasses,
  EChatStatusB2W,
  EMARKETPLACE,
};
