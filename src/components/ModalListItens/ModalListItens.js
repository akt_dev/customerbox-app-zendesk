/* eslint-disable eqeqeq */
/* eslint-disable no-param-reassign */
/* eslint-disable prefer-destructuring */
/* eslint-disable guard-for-in */
/* eslint-disable no-restricted-syntax */
/* eslint-disable no-undef */
/* eslint-disable react/prop-types */
import React, { useState, useEffect } from 'react';
import {
  Typography,
  Grid,
} from '@material-ui/core';
import { AiOutlineShoppingCart } from 'react-icons/ai';
import { makeStyles } from '@material-ui/core/styles';

import ZAF from '../../Services/Zendesk';

import Products from '../../shared/components/Products/Products';

const useStyles = makeStyles(() => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
  },
  title: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
    flexDirection: 'row',
    marginBottom: '10px',
  },
  primaryText: {
    color: '#993fca',
    padding: '0 5px 0 0',
  },
  icon: {
    width: '20px',
    height: '20px',
    position: 'relative',
    top: '4px',
    padding: '0 5px 0 0px',
  },
  items: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  product: {
    overflow: 'auto',
    lineBreak: 'anywhere',
    width: '100%',
    maxWidth: '300px',
    height: '140px',
    backgroundColor: '#F8E4E7',
    padding: '10px',
    borderRadius: '10px',
    marginBottom: '10px',
    marginLeft: '10px',
  },
}));

const ModalListItens = ({ settings, itemsDefault = [], clientDefault = '' }) => {
  const classes = useStyles();
  const [itens, setItens] = useState(itemsDefault);
  const [clientData, setClientData] = useState(clientDefault);
  const { client } = ZAF;

  const parseParams = (paramString) => {
    let paramSub = paramString.replace('#', '').split('&');
    paramSub = paramSub.join('').split('=');
    const paramObj = {};
    paramObj[paramSub[0]] = paramSub[1];
    return paramObj;
  };

  const getParentClient = (parentGuid) => client.instance(parentGuid);

  const init = () => {
    if (window.location.hash) {
      const params = parseParams(window.location.hash);
      const pc = getParentClient(params.parent_guid);
      pc.get('ticket').then(async (ticketData) => {
        const ticketInformations = await client.request({
          url: `/api/v2/tickets/${ticketData.ticket.id}`,
          type: 'GET',
          dataType: 'json',
          headers: { 'Content-Type': 'application/json' },
        });
        const customFields = ticketInformations.ticket.custom_fields;
        // eslint-disable-next-line max-len
        const ticketItens = customFields.filter((cf) => cf.id == settings.customFields.itens_pedido);
        const clientName = customFields.filter((cf) => cf.id == settings.customFields.nome_cliente);
        setItens(JSON.parse(ticketItens[0].value));
        setClientData(clientName[0].value);
      });
    }
  };

  useEffect(() => {
    init();
    client.get('viewport.size').then((data) => {
      const x = !data['viewport.size'] ? 200 : data['viewport.size'].width;
      const y = !data['viewport.size'] ? 200 : data['viewport.size'].height;
      client.invoke('resize', { width: `${Math.trunc(x / 2)}px`, height: `${Math.trunc(y / 2)}px` });
    });
  }, []);

  return (
    <div className={classes.root}>
      <div>
        <b>Cliente:</b>
        {' '}
        {clientData}
      </div>
      <Grid item>
        <div className={classes.title}>
          <Typography
            className={classes.primaryText}
            variant="body1"
          >
            <AiOutlineShoppingCart className={classes.icon} />
            <b>Todos os itens</b>
          </Typography>
        </div>
      </Grid>
      <Products itens={itens} />
    </div>
  );
};

export default ModalListItens;
