export const mock = {
  updated: '01/02/2021 - 16:32:56',
  orderData: {
    customerName: 'Winglerson Silva',
    codeOrder: 12345,
    shipMethod: 'Calango',
    deliveryId: 1235,
    shipCode: 343,
    trackingUrl: 'https://www.calango.com',
    status: 'DELIVERED',
    items: [
      {
        special_price:4.2,
        shipping_cost:38.49,
        sale_fee:null,
        remote_store_id:null,
        qty:1,
        product_id:"1348349",
        original_price:4.2,
        "name":"Chapéu de Calango",
        listing_type_id:null,
        id:"1",
        gift_wrap:null,
        detail:null
      },
      {
        special_price:8.4,
        shipping_cost:48.59,
        sale_fee:null,
        remote_store_id:null,
        qty:2,
        product_id:"4849384",
        original_price:8.4,
        "name":"Chaveiro",
        listing_type_id:null,
        id:"2",
        gift_wrap:null,
        detail:null
      }
    ],
  },
};
