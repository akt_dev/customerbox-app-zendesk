import React from 'react';
import { render } from '@testing-library/react';
import { mock } from './ModalListItens.mock';
import ModalListItens from './ModalListItens';

jest.mock('../../Services/Zendesk', () => ({
  client: {
    get: () => new Promise((res) => res(1)),
    invoke: () => new Promise((res) => res(1)),
  },
}));

describe('ModalList Component', () => {
  it('Renders without crashing', () => {
    render(<ModalListItens itemsDefault={mock.orderData.items} clientDefault={mock.orderData.customerName} />);
  });

  it('props work', () => {
    const { getByText } = render(<ModalListItens itemsDefault={mock.orderData.items} clientDefault={mock.orderData.customerName} />);

    expect(getByText(`${mock.orderData.customerName}`)).toBeInTheDocument();

    mock.orderData.items.map(item => {
      expect(getByText(`${item.name}`)).toBeInTheDocument();
      expect(getByText(`${item.product_id}`)).toBeInTheDocument();
      expect(getByText(`${item.qty}`)).toBeInTheDocument();
    });
  });
});
