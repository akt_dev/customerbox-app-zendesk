/* eslint-disable react/prop-types */
import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Alert from '@material-ui/lab/Alert';
import HistoryIcon from '@material-ui/icons/History';
import {
  Tab,
  Tabs,
} from '@material-ui/core';

import { EChatStatus, EChatStatusClasses, EChatStatusB2W } from '../../enums/modal.enums';

const useStyles = makeStyles((theme) => ({
  buttonSecondary: {
    color: '#993fca',
    borderColor: '#993fca',
    marginRight: theme.spacing(2),
    '&:selected': {
      background: '#bea3ce',
      color: '#fff',
    },
    '&:indicator': {
      background: '#bea3ce',
    },
  },
  alert: {
    borderRadius: '10%',
    marginTop: theme.spacing(1),
  },
}));

const ActionButtons = ({ handleButtonChatType, statusGroup, handleMoreDetails }) => {
  const classes = useStyles();
  const [tabActive, setTabActive] = useState('customSeller');
  const [status, setStauts] = useState(null);

  const handleTabSelected = (event, newValue) => {
    setTabActive(newValue);
    handleButtonChatType(newValue);
  };

  useEffect(() => {
    if (statusGroup && statusGroup.length > 0) {
      let { status: statusChat } = statusGroup.filter((value) => value.type === tabActive)[0];
      if (statusChat === 1) statusChat = EChatStatusB2W.ACTIVE;

      setStauts(() => EChatStatus[`${statusChat}`].toUpperCase());
    }
  }, [tabActive, statusGroup]);

  return (
    <div style={{ display: 'flex', flexDirection: 'column' }}>
      <Tabs
        value={tabActive}
        className={classes.buttonSecondary}
        onChange={handleTabSelected}
      >
        <Tab data-testid="tab-customSeller" label="Atendimento" value="customSeller" />
        <Tab data-testid="tab-b2w" label="Ajuda B2W" value="b2w" />
        <Tab data-testid="tab-normal" label="Mediação B2W" value="normal" />
      </Tabs>
      <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'space-between' }}>
        <Alert severity={EChatStatusClasses[`${status}`]} className={classes.alert}>
          { status || ''}
        </Alert>
        <Alert
          icon={<HistoryIcon fontSize="inherit" />}
          severity="success"
          className={classes.alert}
          style={{ cursor: 'pointer' }}
          onClick={handleMoreDetails}
        >
          Ver histórico de atendimento
        </Alert>
      </div>
    </div>
  );
};

export default ActionButtons;
