import React from 'react';
import { render, act } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import ModalChat from './ModalChat';

jest.mock('../../Services/Zendesk', () => ({
  getSettings: () => [jest.fn()],
  modal: () => new Promise((res) => res(1)),
  size: (width, height) => null,
  resize: (width, height) => null,
  client: {
    get: () => new Promise((res) => res(1)),
    on: () => new Promise((res) => res(1)),
  },
  scrollRef: {
    current: {
      scrollIntoView: () => {}
    }
  }
}));

describe('ModalChat component', () => {
  it('Renders without crashing', () => {
    render(<ModalChat />);
  });

  it('Tab navigation', async () => {
    const {
      getByTestId,
    } = render(<ModalChat />);

    // Click on tab
    await act(async () => {
      userEvent.click(getByTestId('tab-customSeller'));
    });
    // Check if it is on focus
    expect(getByTestId('tab-customSeller')).toHaveFocus();

    // Click on tab
    await act(async () => {
      userEvent.click(getByTestId('tab-normal'));
    });
    // Check if it is on focus
    expect(getByTestId('tab-normal')).toHaveFocus();

    // Click on tab
    await act(async () => {
      userEvent.click(getByTestId('tab-b2w'));
    });
    // Check if it is on focus
    expect(getByTestId('tab-b2w')).toHaveFocus();
  });

  it.todo('Send menssage');

  it('Send message only with text is not null or empty', async () => {
    const {
      getByTestId,
    } = render(<ModalChat />);

    expect(getByTestId('icon-send-message')).toBeDisabled();

    await act(async () => {
      const inputMessage = getByTestId('input-send-message');
      userEvent.type(inputMessage, 'Meu texto legal...');
    });
    expect(getByTestId('icon-send-message')).toBeEnabled();
  });

  it('Should block input message chat if Mediação B2W is blocked', async () => {
    const {
      getByTestId,
    } = render(<ModalChat />);

    // Click on tab
    await act(async () => {
      userEvent.click(getByTestId('tab-normal'));
    });
    expect(getByTestId('icon-send-message')).toBeDisabled();
  })

  it.todo('Should open new window with chat history')
});
