/* eslint-disable no-shadow */
/* eslint-disable react/prop-types */
/* eslint-disable guard-for-in */
/* eslint-disable no-restricted-syntax */
import React, { useState, useEffect } from 'react';

import { BsArrowReturnLeft } from 'react-icons/bs';

// MATERIAL UI
import {
  Grid,
  TextField,
  InputAdornment,
  IconButton,
  Typography,
} from '@material-ui/core';
import SendIcon from '@material-ui/icons/Send';
import { makeStyles } from '@material-ui/core/styles';

// SERVICES, HELPERS AND ENUMS
import {
  ModalSize, ETypes, ETypesB2W, EChatStatusB2W,
} from '../../enums/modal.enums';
import {
  filterMessages,
} from '../../helpers/chats.helper';
import { localeIsoDateNow } from '../../helpers/time.helper';
// eslint-disable-next-line no-unused-vars
import { sendMessage, endChat } from '../../Services/CustomerBox';
import Zaf from '../../Services/Zendesk';

// COMPONENTS
import Chat from './Chat';
import Order from '../../shared/components/Order/Order';
import CustomButton from '../../shared/components/CustomButton/CustomButton';
import ActionButtons from './ActionButtons';
import Progress from '../../shared/components/Progress/Progress';
import Products from '../../shared/components/Products/Products';

const useStyles = makeStyles((theme) => ({
  contentChat: {
    height: '100vh',
    order: 2,
    flexDirection: 'column',
    display: 'flex',
  },
  contentModal: {
    height: '100vh',
    display: 'flex',
    flexDirection: 'row',
  },
  contentConfig: {
    height: '100vh',
    order: 1,
  },
  contentMessages: {
    backgroundColor: '#f6f6f6',
    order: 2,
    overflow: 'auto',
    flexGrow: 2,
    margin: theme.spacing(1),
  },
  buttonSecondary: {
    color: '#993fca',
    borderColor: '#993fca',
    marginRight: theme.spacing(2),
    '&:hover': {
      background: '#bea3ce',
      color: '#fff',
    },
  },
  actionButtonsPainel: {
    order: 1,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#fff',
    paddingBottom: theme.spacing(1),
  },
  chatInput: {
    backgroundColor: '#fff',
    margin: theme.spacing(1),
    order: 3,
    flexGrow: 1,
  },
  iconButton: {
    padding: 10,
    fontSize: '44px',
  },
  divider: {
    height: 28,
    margin: 4,
  },
  rootList: {
    display: 'flex',
    flexDirection: 'column',
    height: '10%',
  },
  title: {
    cursor: 'pointer',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
    flexDirection: 'row',
    marginBottom: '10px',
    marginLeft: '20px',
  },
  primaryText: {
    color: '#993fca',
    padding: '0 5px 0 0',
    fontWeight: 'bold',
  },
  icon: {
    width: '20px',
    height: '20px',
    position: 'relative',
    top: '4px',
    padding: '0 5px 0 0px',
  },
}));

let ticketId;
let codeOrder;
let updated;
let customFieldChatId;

const ModalChatB2W = ({ settings }) => {
  const [allMessages, setAllMessages] = useState([]);
  const [chatHistory, setChatHistory] = useState([]);
  const [chatType, setChatType] = useState('customSeller');
  const [chatMessage, setChatMessage] = useState('');
  const [orderData, setOrderData] = useState(null);
  const [originalPayload, setOriginalPayload] = useState('');
  const [statusGroup, setStatusGroup] = useState({});
  const [label, setLabel] = useState('Digite sua mensagem aqui');
  const [errorMessage, setErrorMessage] = useState(false);
  const [disabledMessage, setDisabledMessage] = useState(false);
  const [listItens, setListItens] = useState(false);

  const classes = useStyles();

  const updateListItens = () => setListItens(true);

  const updateComment = (chatMessages) => {
    Zaf.updateCustomField({
      ticketId,
      customFieldId: customFieldChatId,
      customFieldValue: chatMessages,
    });
  };

  const handleLabelError = ((newLabel, time) => {
    setLabel(newLabel);
    setErrorMessage(true);
    setDisabledMessage(true);
    setTimeout(() => {
      setLabel('Digite sua mensagem aqui');
      setErrorMessage(false);
      setDisabledMessage(false);
    }, time);
  });

  // TODO REFACTOR
  const handleMessage = ({ newMessage, status }) => {
    let tempPayload = JSON.parse(originalPayload);
    let position = JSON.parse(originalPayload)
      .findIndex((el) => el.parts === ETypesB2W[`${chatType}`]);

    if (position < 0) {
      position = JSON.parse(originalPayload).length;
      tempPayload.push({
        last_message: localeIsoDateNow(),
        messages: [],
        parts: ETypesB2W[`${chatType}`],
        status,
      });
    }

    tempPayload[position].messages.push(newMessage);
    tempPayload[position].status = status;
    tempPayload = JSON.stringify(tempPayload);
    setOriginalPayload(tempPayload);
    allMessages[`${chatType}`].messages.push(newMessage);
    sendMessage({
      message: chatMessage,
      parts: ETypesB2W[`${chatType}`],
      orderId: orderData.codeOrder,
      deliveryId: orderData.deliveryId,
    }).then(() => {
      setDisabledMessage(false);
      setLabel('Digite sua menssagem aqui');
      setChatMessage('');
      let tempPayload = JSON.parse(originalPayload);
      let position = JSON.parse(originalPayload)
        .findIndex((el) => el.parts === ETypesB2W[`${chatType}`]);

      if (position < 0) {
        position = JSON.parse(originalPayload).length;
        tempPayload.push({
          last_message: localeIsoDateNow(),
          messages: [],
          parts: ETypesB2W[`${chatType}`],
          status: 'ACTIVE',
        });
      }
      tempPayload[position].messages.push(newMessage);
      tempPayload = JSON.stringify(tempPayload);
      setOriginalPayload(tempPayload);
      allMessages[`${chatType}`].messages.push(newMessage);

      updateComment(tempPayload);
    }).catch(() => {
      handleLabelError('Falha ao enviar mensagem.', 10000);
    });
    updateComment(tempPayload);
  };

  const changeStatus = ({ status }) => {
    setStatusGroup(() => (
      statusGroup.map((value) => {
        if (value.type === chatType) {
          // eslint-disable-next-line no-param-reassign
          value.status = status;
        }
        return value;
      })
    ));
  };

  const handleSendMessage = () => {
    setChatMessage('');

    changeStatus({ status: EChatStatusB2W.ACTIVE });

    const newMessage = {
      content: chatMessage,
      created_at: localeIsoDateNow(),
      part: {
        code: settings.currentUser.id,
        email: settings.currentUser.email,
        name: settings.currentUser.name,
        role: 'EMPLOYEE',
      },
    };
    setLabel('Enviando mensagem...');
    setDisabledMessage(true);
    handleMessage({ newMessage, status: EChatStatusB2W.ACTIVE });
  };

  const handleEndChat = () => {
    endChat({ code: orderData.codeOrder });
    setChatMessage('');

    changeStatus({ status: EChatStatusB2W.ARCHIVED });

    const newMessage = {
      content: 'Atendimento Encerrado',
      created_at: localeIsoDateNow(),
      part: {
        code: settings.currentUser.id,
        email: settings.currentUser.email,
        name: settings.currentUser.name,
        role: 'INFO',
      },
    };

    handleMessage({ newMessage, status: EChatStatusB2W.ARCHIVED });
  };

  const handleKeyPress = (key) => {
    if (key === 'Enter') {
      handleSendMessage();
    }
  };

  const handleMoreDetails = () => {
    window.open(`${settings.customFields.b2w_url_detalhes}/${orderData.deliveryId}`);
  };

  // Click on tabs for chat types
  const handleButtonChatType = (type) => {
    setChatType(type);
    setDisabledMessage(false);
    if (type === ETypes.NORMAL && allMessages.normal) {
      setChatHistory(allMessages.normal.messages);
      // Se "mediação b2w" is blocked for messages
      if (typeof allMessages.normal.status === 'number') {
        setDisabledMessage(true);
      }
    }
    if (type === ETypes.SELLER && allMessages.b2w) {
      setChatHistory(allMessages.b2w.messages);
    }
    if (type === ETypes.CUSTOMER_SELLER && allMessages.customSeller) {
      setChatHistory(allMessages.customSeller.messages);
    }
  };

  useEffect(() => {
    const { client } = Zaf;
    Zaf.resize(ModalSize.WIDTH, ModalSize.HEIGHT);
    Zaf.client.get('instances')
      .then((instancesData) => {
        const { instances } = instancesData;
        const appClients = [];
        for (const instanceGuid in instances) {
          if (
            instances[instanceGuid].location === 'ticket_sidebar'
            || instances[instanceGuid].location === 'new_ticket_sidebar'
          ) {
            appClients.push(client.instance(instanceGuid));
          }
        }
        return appClients;
      })
      .then((appClients) => {
        for (const i in appClients) {
          try {
            appClients[i].trigger('get_info_ticket');
            // eslint-disable-next-line no-console
          } catch (err) { console.error('Something went wrong...'); }
        }
      });
    Zaf.client.on('info-modal-customerbox', (data) => {
      if (data) {
        // instanceGuidId = data.instanceGuidId;
        ticketId = data.ticketId;
        codeOrder = data.codeOrder;
        updated = data.updated;
        customFieldChatId = data.customFieldChatId;
        setOrderData({
          items: data.items,
          codeOrder,
          ticketMediation: data.ticketMediation,
          shipCode: data.shipCode,
          shipMethod: data.shipMethod,
          status: data.status,
          customerName: data.customerName,
          trackingUrl: data.trackingUrl,
          codeDelivered: data.codeDelivered,
          deliveryId: data.deliveryId,
        });
        setOriginalPayload(data.comments);
        const messagesChat = filterMessages(data);

        setAllMessages(messagesChat);
        setChatHistory(messagesChat.customSeller.messages);
        setStatusGroup([
          { type: 'normal', status: messagesChat.normal.status },
          { type: 'b2w', status: messagesChat.b2w.status },
          { type: 'customSeller', status: messagesChat.customSeller.status },
        ]);
      }
    });
  }, []);

  return (
    <>
      <Grid className={classes.contentModal}>
        <Grid
          item
          xs={4}
          className={classes.contentModal}
          style={{
            paddingTop: '6vh',
            paddingLeft: '1vh',
            borderRight: '1px solid #8e9091',
            padding: '0.5em',
          }}
        >
          <Grid>
            {orderData
              ? (
                <Order
                  key={+new Date()}
                  updated={updated}
                  orderData={orderData}
                  listItens={updateListItens}
                />
              ) : (<Progress />)}

            <Grid
              item
              xs={12}
              style={{ display: 'flex', flex: 1, justifyContent: 'space-between' }}
            >
              <CustomButton
                secondary
                title="Ver detalhes"
                onClick={handleMoreDetails}
                style={{ marginRight: '10px' }}
              />
              <CustomButton
                title="Finalizar Atedimento"
                onClick={handleEndChat}
              />
            </Grid>
          </Grid>
        </Grid>

        {listItens
          ? (
            <Grid item xs={8} className={classes.rootList}>
              <div className={classes.title}>
                <Typography
                  className={classes.primaryText}
                  variant="body1"
                  onClick={() => setListItens(false)}
                >
                  <BsArrowReturnLeft className={classes.icon} />
                  Voltar ao chat
                </Typography>
              </div>
              <Products itens={JSON.parse(orderData.items)} />
            </Grid>
          ) : (
            <Grid item xs={8} className={classes.contentChat}>
              <Grid className={classes.actionButtonsPainel}>
                <ActionButtons
                  handleButtonChatType={handleButtonChatType}
                  handleMoreDetails={handleMoreDetails}
                  statusGroup={statusGroup}
                />
              </Grid>
              <Grid className={classes.contentMessages}>
                <Grid style={{ height: '80vh', flexGrow: 2, flexWrap: 'wrap' }}>
                  <Chat
                    chatHistory={chatHistory}
                  />
                </Grid>
              </Grid>
              <Grid className={classes.chatInput}>
                <TextField
                  label={label}
                  fullWidth
                  multiline
                  error={errorMessage}
                  disabled={(orderData && orderData.ticketMediation) || disabledMessage}
                  rows={4}
                  rowsMax={4}
                  size="small"
                  variant={(orderData && orderData.ticketMediation) ? 'filled' : 'outlined'}
                  value={chatMessage}
                  onChange={(event) => setChatMessage(event.target.value)}
                  onKeyPress={(event) => handleKeyPress(event.key)}
                  InputProps={{
                    'data-testid': 'input-send-message',
                    endAdornment:
              <InputAdornment position="end">
                <IconButton
                  data-testid="icon-send-message"
                  color="primary"
                  className={classes.iconButton}
                  aria-label="directions"
                  onClick={handleSendMessage}
                  disabled={!chatMessage}
                >
                  <SendIcon fontSize="large" />
                </IconButton>
              </InputAdornment>,
                  }}
                />
              </Grid>
            </Grid>
          )}
      </Grid>
    </>
  );
};

export default ModalChatB2W;
