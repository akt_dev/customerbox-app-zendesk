export const mock = {
  customFields: {
    updated: '01/02/2021 - 16:32:56',
    customerName: 'Winglerson Silva',
    codeOrder: 12345,
    shipMethod: 'Calango',
    deliveryId: 1235,
    shipCode: 343,
    trackingUrl: 'https://www.calango.com',
    status: 'DELIVERED',
    items: '[{"special_price":4.2,"shipping_cost":38.49,"sale_fee":null,"remote_store_id":null,"qty":1,"product_id":"1","original_price":4.2,"name":"Chapéu de Calango","listing_type_id":null,"id":"1","gift_wrap":null,"detail":null}]',
    b2w_url_detalhes: 'https://www.b2wmarketplace.com.br/v3/sac/atendimento/detalhe',
  }
};
