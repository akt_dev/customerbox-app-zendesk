/* eslint-disable react/prop-types */
import React, { useEffect, useRef } from 'react';
import {
  List,
  ListItem,
  ListItemText,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  CUSTOMER: {
    borderRadius: '20px',
    backgroundColor: '#A556D0',
    padding: `${theme.spacing(6)} ${theme.spacing(2)}`,
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(2),
    display: 'block',
    maxWidth: '90vh',
    left: '2vh',
    right: '90vh',
    overflowWrap: 'break-word',
  },
  EMPLOYEE: {
    borderRadius: '20px',
    backgroundColor: '#CEA2E5',
    padding: `${theme.spacing(2)} ${theme.spacing(2)}`,
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(2),
    display: 'block',
    maxWidth: '90vh',
    left: '45vh',
    overflowWrap: 'break-word',
  },
  SELLER: {
    borderRadius: '20px',
    backgroundColor: '#aa7fc1',
    padding: `${theme.spacing(2)} ${theme.spacing(2)}`,
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(2),
    display: 'block',
    maxWidth: '90vh',
    left: '45vh',
    overflowWrap: 'break-word',
  },
  INFO: {
    borderRadius: '10px',
    backgroundColor: '#7f7286',
    color: 'white',
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(2),
    display: 'block',
    maxWidth: '0vh',
    left: '20vh',
    minWidth: '65%',
    textAlign: 'center',
    fontWeight: '700',
  },
  messageMetaInfo: {
    '& .MuiListItemText-secondary': {
      color: '#fff',
      marginTop: theme.spacing(1),
      textAlign: 'right',
    },
  },
}));

const Chat = ({ chatHistory }) => {
  const classes = useStyles();
  const scrollRef = useRef(null);

  useEffect(() => {
    if (scrollRef.current && scrollRef.current.scrollIntoView) {
      scrollRef.current.scrollIntoView({ behaviour: 'smooth' });
    }
  });
  return (
    <List data-testid="chat-container">
      {chatHistory && chatHistory.length > 0
        ? chatHistory.map((message, index) => {
          if (message.content) {
            const dateLocale = new Date(message.created_at).toLocaleDateString('pt-Br');
            const timeLocale = new Date(message.created_at).toLocaleTimeString('pt-Br');
            return (
              <ListItem
                // eslint-disable-next-line react/no-array-index-key
                key={index}
                data-testid="chat-message"
                ref={scrollRef}
                className={classes[`${message.part.role}`]}
              >
                <ListItemText
                  className={classes.messageMetaInfo}
                  primary={message.content}
                  secondary={`${message.part.name} - ${dateLocale} ${timeLocale}`}
                />
              </ListItem>
            );
          }
          return [];
        })
        : []}
    </List>
  );
};

export default Chat;
