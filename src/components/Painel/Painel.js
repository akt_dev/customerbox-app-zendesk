/* eslint-disable no-restricted-syntax */
/* eslint-disable react/prop-types */
import React, { useState } from 'react';
import {
  Grid,
} from '@material-ui/core';

import { makeStyles } from '@material-ui/core/styles';
import CustomButton from '../../shared/components/CustomButton/CustomButton';

import Order from '../../shared/components/Order/Order';
import Progress from '../../shared/components/Progress/Progress';
import { EMARKETPLACE } from '../../enums/modal.enums';
import { usePainelHooks } from './painel.hooks';

import Zaf from '../../Services/Zendesk';

const useStyles = makeStyles(() => ({
  gridMargin: {
    display: 'flex',
    flexDirection: 'column',
  },
  primaryText: {
    color: '#993fca',
  },
}));

const Painel = ({
  codeOrder,
  shipMethod,
  shipCode,
  status,
  items,
  customerName,
  trackingUrl,
  codeDelivered,
  deliveryId,
  detailLink,
  customFieldChat,
  marketplaceType,
  ticketMediation,
}) => {
  const classes = useStyles();
  const [instanceModalId, setInstanceModalId] = useState(null);

  const settings = {
    codeOrder,
    shipMethod,
    shipCode,
    status,
    items,
    customerName,
    trackingUrl,
    codeDelivered,
    deliveryId,
    marketplaceType,
    ticketMediation,
  };

  const [
    updated,
    orderData,
  ] = usePainelHooks({ settings, instanceModalId, customFieldChat });

  // Open specific modal for marketplace type
  const handleOpenModal = () => {
    Zaf.modal(`chat[${marketplaceType}]`).then((id) => {
      setInstanceModalId(() => id);
    });
  };
  // Open specific order details for marketplace type
  const handleMoreDetails = () => {
    // Order detail for B2W marketplace
    if (marketplaceType === EMARKETPLACE.B2W) {
      window.open(`${detailLink}/${orderData.deliveryId}`);
    }
  };

  return (
    <>
      { orderData
        ? (
          <Grid item className={classes.gridMargin}>
            <Order
              style={{ flexGrow: 2, order: 1 }}
              key={+new Date()}
              updated={updated}
              orderData={orderData}
            />
            <Grid container style={{ flexGrow: 1, order: 2 }}>
              <Grid item xs={6}>
                <CustomButton
                  onClick={handleOpenModal}
                  title="Chat com cliente"
                />
              </Grid>
              <Grid item xs={6}>
                <CustomButton
                  secondary
                  onClick={handleMoreDetails}
                  title="Mais detalhes no marketplace"
                />
              </Grid>
            </Grid>
          </Grid>
        )
        : (<Progress />)}
    </>
  );
};

export default Painel;
