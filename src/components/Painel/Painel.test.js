import React from 'react';
import { render, fireEvent, act } from '@testing-library/react';
import Painel from './Painel';
import { mock } from './Painel.mock';
import { EMARKETPLACE } from '../../enums/modal.enums';

jest.mock('../../Services/Zendesk', () => ({
  getSettings: () => [jest.fn()],
  modal: () => new Promise((res) => res(1)),
  client: {get: () => new Promise((res) => res(1)),}
}));

jest.mock('./painel.hooks', () => ({
  usePainelHooks: () => [mock.update, mock.orderData, jest.fn()],
}));

describe('Painel Component', () => {
  it('Renders without crashing', () => {
    render(<Painel />);
  });

  it('Should open a modal with chat to B2W Marketplace', async () => {
    // jest.spyOn(jest.fn, 'modal').mockImplementation(() => true);
    const { getByText } = render(
      <Painel
        settings={mock.settings}
        deliveryId={mock.orderData.deliveryId}
        detailLink={mock.settings.b2w_url_detalhes}
        marketplaceType={EMARKETPLACE.B2W}
      />,
    );

    global.open = jest.fn();

    await act(async () => {
      // Simula evento do click no botão com esse texto
      fireEvent.click(getByText('Mais detalhes no marketplace'));
    });

    expect(global.open).toHaveBeenCalledTimes(1);
    expect(global.open).toHaveBeenCalledWith('https://www.b2wmarketplace.com.br/v3/sac/atendimento/detalhe/1235');
  });
});
