/* eslint-disable no-restricted-syntax */
import { useState, useEffect } from 'react';
import Zaf from '../../Services/Zendesk';
// import useComments from '../../hooks/useComments';

export function usePainelHooks({ settings, instanceModalId, customFieldChat }) {
  const [updated, setUpdated] = useState('');
  const [orderData, setOrderData] = useState(null);
  const [ticketId, setTicketId] = useState(null);
  const [instanceGuidId, setInstanceGuidId] = useState(null);
  const [chatMessages, setChatMessages] = useState('[]');

  // const [updatedAt] = useComments();

  const updatedAt = '2021-01-01';
  useEffect(() => {
    const {
      codeOrder,
      shipMethod,
      shipCode,
      status,
      items,
      customerName,
      trackingUrl,
      codeDelivered,
      deliveryId,
      ticketMediation,
    } = settings;

    setOrderData({
      codeOrder,
      shipMethod,
      shipCode,
      status,
      items,
      customerName,
      trackingUrl,
      codeDelivered,
      deliveryId,
    });
    setUpdated(
      `${new Date(updatedAt).toLocaleDateString('pt-br')} -
            ${new Date(updatedAt).toLocaleTimeString('pt-br')}`,
    );

    Zaf.getTicketInfo().then((data) => setTicketId(data.id));

    Zaf.client.get(`ticket.customField:custom_field_${customFieldChat}`)
      .then((data) => {
        if (data) {
          setChatMessages(() => data[`ticket.customField:custom_field_${customFieldChat}`]);
        }
      });

    if (settings) {
      Zaf.client.get('instances').then((instancesData) => {
        const { instances } = instancesData;
        for (const instanceGuid in instances) {
          if (instances[instanceGuid].location === 'ticket_sidebar') {
            // eslint-disable-next-line no-underscore-dangle
            const guidId = Zaf.client.instance(instanceGuid)._instanceGuid;
            setInstanceGuidId(guidId);
          }
        }
      });

      Zaf.client.on('get_info_ticket', () => {
        if (instanceModalId) {
          Zaf.client.instance(instanceModalId)
            .trigger(
              'info-modal-customerbox',
              {
                comments: chatMessages,
                instanceGuidId,
                ticketId,
                codeOrder,
                shipMethod,
                shipCode,
                status,
                items,
                customerName,
                trackingUrl,
                codeDelivered,
                updated,
                deliveryId,
                ticketMediation,
                customFieldChatId: customFieldChat,
              },
            );
        }
      });
    }

    return () => {
      // eslint-disable-next-line no-param-reassign
      instanceModalId = null;
    };
  }, [instanceModalId]);

  return [updated, orderData];
}
