/* eslint-disable react/prop-types */
import React from 'react';
import './Sidebar.css';
import { Typography } from '@material-ui/core';
import useSidebarHooks from './Sidebar.hooks';

import Painel from '../Painel/Painel';

export default function Sidebar({ settings }) {
  const [
    codeOrder,
    shipMethod,
    // eslint-disable-next-line no-unused-vars
    shipCode,
    status,
    items,
    customerName,
    trackingUrl,
    codeDelivered,
    deliveryId,
    marketplaceType,
    ticketMediation,
  ] = useSidebarHooks({ settings });

  return (
    <>
      { settings.customFields && (codeOrder && status)
        ? (
          <Painel
            codeOrder={codeOrder}
            shipMethod={shipMethod}
            shipCode={codeDelivered}
            status={status}
            items={items}
            customerName={customerName}
            trackingUrl={trackingUrl}
            codeDelivered={codeDelivered}
            deliveryId={deliveryId}
            detailLink={settings.customFields.b2w_url_detalhes}
            currentUser={settings.currentUser}
            customFieldChat={settings.customFields.chat_customerbox}
            marketplaceType={marketplaceType}
            ticketMediation={ticketMediation}
          />
        )
        : (
          <Typography>
            Oooops! Parece que houve um erro aqui...
          </Typography>
        )}
    </>
  );
}
