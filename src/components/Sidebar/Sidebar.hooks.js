import { useEffect, useState } from 'react';
import Zaf from '../../Services/Zendesk';

const useSidebarHooks = ({ settings }) => {
  const [codeOrder, setCodeOrder] = useState(null);
  const [shipMethod, setShipMethod] = useState(null);
  const [shipCode, setShipCode] = useState(null);
  const [status, setStatus] = useState(null);
  const [items, setItems] = useState(null);
  const [customerName, setCustomerName] = useState(null);
  const [trackingUrl, setTrackingUrl] = useState(null);
  const [codeDelivered, setCodeDelivered] = useState(null);
  const [deliveryId, setDeliveryId] = useState(null);
  const [marketplaceType, setMarketplaceType] = useState(null);
  const [ticketMediation, setTicketMediation] = useState(null);

  /**
   * Resive and save ticket
   */
  useEffect(() => {
    Zaf.resize('100%', '70vh');
    Zaf.client.on('ticket.save', () => new Promise((resolve) => {
      resolve();
    }));
  }, []);

  /**
   * Load custom fields values from settings (props)
   */
  useEffect(() => {
    if (settings.customFields) {
      const customFields = [
        { id: 0, name: 'atendimento_b2w', field: settings.customFields.atendimento_b2w },
        { id: 1, name: 'codigo_entrega', field: settings.customFields.codigo_entrega },
        { id: 2, name: 'codigo_transporte', field: settings.customFields.codigo_transporte },
        { id: 3, name: 'cpf_cliente', field: settings.customFields.cpf_cliente },
        { id: 4, name: 'deliveryId', field: settings.customFields.deliveryId },
        { id: 5, name: 'itens_pedido', field: settings.customFields.itens_pedido },
        { id: 6, name: 'link_tracking', field: settings.customFields.link_tracking },
        { id: 7, name: 'nome_cliente', field: settings.customFields.nome_cliente },
        { id: 8, name: 'status_pedido', field: settings.customFields.status_pedido },
        { id: 9, name: 'tipo_transporte', field: settings.customFields.tipo_transporte },
        { id: 10, name: 'chat_customerbox', field: settings.customFields.chat_customerbox },
        { id: 11, name: 'marketplace_type', field: settings.customFields.marketplace_type },
        { id: 12, name: 'mediacao_ticket', field: settings.customFields.mediacao_ticket },
      ];
      Zaf.client.get(
        customFields.map((field) => `ticket.customField:custom_field_${field.field}`),
      )
        .then((data) => {
          if (data) {
            const arrayResult = Object.entries(data).map((field) => ({
              id: field[0].replace('ticket.customField:custom_field_', ''),
              value: field[1],
            }));
            // Indexes of array are in manifest.json
            setCodeOrder(() => arrayResult[1].value);
            setCodeDelivered(() => arrayResult[2].value);
            setShipCode(() => arrayResult[4].value);
            setDeliveryId(() => arrayResult[5].value);
            setItems(() => arrayResult[6].value);
            setTrackingUrl(() => arrayResult[7].value);
            setCustomerName(() => arrayResult[8].value);
            setStatus(() => arrayResult[9].value);
            setShipMethod(() => arrayResult[10].value);
            setMarketplaceType(() => arrayResult[12].value);
            setTicketMediation(() => arrayResult[13].value === 'yes');
          }
        });
    }
  });
  return [
    codeOrder,
    shipMethod,
    shipCode,
    status,
    items,
    customerName,
    trackingUrl,
    codeDelivered,
    deliveryId,
    marketplaceType,
    ticketMediation,
  ];
};

export default useSidebarHooks;
