import React from 'react';
import { render, act } from '@testing-library/react';
import Sidebar from './Sidebar';
import { mock } from './Sidebar.mock';

jest.mock('../../Services/Zendesk', () => ({
  getSettings: () => [jest.fn()],
  modal: () => new Promise((res) => res(1)),
  client: {
    on: () => new Promise((res) => res(1)),
  },
  getTicketInfo: () => new Promise((res) => res(1)),
  resize: () => new Promise((res) => res(1)),
}));

describe('Sidebar Component', () => {
  it('Renders without crashing', async () => {
    await act(async () => {
      render(<Sidebar settings={mock.settings}/>)
    });

  });
});
