/* eslint-disable no-case-declarations */
import React, { useEffect, useState } from 'react';
import 'react-notifications/lib/notifications.css';
import { NotificationContainer } from 'react-notifications';
import Zaf from './Services/Zendesk';
import { EMARKETPLACE } from './enums/modal.enums';

import Sidebar from './components/Sidebar/Sidebar';
import ModalListItens from './components/ModalListItens/ModalListItens';
import ModalChatB2W from './components/ModalChatB2W/ModalChat';

const search = window.location.search.match(/(type|modal)=([a-z_]+)/);
const marketplace = window.location.search.match(/\[(.*?)\]/);

export default function App() {
  const [settings, setSettings] = useState(null);
  const [page, setPage] = useState(null);

  useEffect(() => {
    Zaf.client.get('currentUser').then((current) => {
      Zaf.getSettings().then((data) => {
        setSettings(() => ({
          customFields: data,
          currentUser: current.currentUser,
        }));
      });
    });
  }, []);

  // Open modals chats for marketplaces
  const opanMarketplaceChat = () => {
    if (marketplace.length > 0 && marketplace[1] === EMARKETPLACE.B2W) {
      setPage(<ModalChatB2W settings={settings} />);
    }
  };

  useEffect(() => {
    if (settings) {
      switch (search[2]) {
        case 'sidebar':
          setPage(<Sidebar settings={settings} />);
          break;
        case 'chat':
          opanMarketplaceChat();
          break;
        case 'list':
          setPage(<ModalListItens settings={settings} />);
          break;
        default:
      }
    }
  }, [settings]);

  return (
    <>
      <React.Suspense fallback="Carregando...">
        {settings ? page : null}
      </React.Suspense>
      <NotificationContainer />
    </>
  );
}
