// eslint-disable-next-line arrow-body-style
const localeIsoDateNow = () => {
  const fakeLocale = new Date()
    .toLocaleString('en-US', { timeZone: 'America/El_Salvador' });
  return new Date(fakeLocale).toISOString().split('.')[0];
};

export {
  localeIsoDateNow,
};
