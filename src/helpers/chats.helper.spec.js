import { mockChat } from './chat.mock';
import * as helper from './chats.helper';

describe('Chat Messages Helpers Suit', () => {
  test('Should returns a valid object', () => {
    const result = helper.filterMessages(mockChat);
    expect(Object.keys(result)).toEqual(['normal', 'b2w', 'customSeller']);
    expect(Object.keys(result.customSeller)).toEqual(['messages', 'status']);
    expect(Object.keys(result.b2w)).toEqual(['messages', 'status']);
    expect(Object.keys(result.normal)).toEqual(['messages', 'status']);
  });
});
