import { EChats } from '../enums/modal.enums';

const filterMessages = (data) => {
  if (data && data.comments.length > 0) {
    const normal = { messages: [], status: 1 };
    const b2w = { messages: [], status: 1 };
    const customSeller = { messages: [], status: 1 };
    if (typeof data.comments === 'string') {
      // eslint-disable-next-line no-param-reassign
      data.comments = JSON.parse(data.comments || '[]');
    }

    data.comments.forEach((value) => {
      if (value.parts === EChats.NORMAL) {
        normal.messages.push(...value.messages);
        normal.status = value.status;
      }
      if (value.parts === EChats.SELLER) {
        b2w.messages.push(...value.messages);
        b2w.status = value.status;
      }
      if (value.parts === EChats.CUSTOMER_SELLER) {
        customSeller.messages.push(...value.messages);
        customSeller.status = value.status;
      }
    });
    return {
      normal,
      b2w,
      customSeller,
    };
  }

  return [];
};

export {
  filterMessages,
};
