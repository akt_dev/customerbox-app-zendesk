import React from 'react';
import { render, act } from '@testing-library/react';

import App from './App';

jest.mock('./Services/Zendesk', () => ({
  getSettings: () => ({ then: jest.fn(() => {})}),
  modal: () => new Promise((res) => res(1)),
  size: (width, height) => null,
  resize: (width, height) => null,
  client: {
    get: () => new Promise((res) => res(1)),
    on: () => new Promise((res) => res(1)),
  },
  scrollRef: {
    current: {
      scrollIntoView: () => {}
    }
  }
}));

describe('App component', () => {
  it('renders without crashing', () => {
    render(<App />)
  });
  it('should opens b2w modal chat', () => {
    global.window = Object.create(window);
    const search = "?type=sidebar&origin=https%3A%2F%2Fpartner-devs3-aktienow.zendesk.com&app_guid=5df51018-d2fe-49a9-8611-2e55e9250c48";
    Object.defineProperty(window, 'search', {
      value: search,
    });

    render(<App />)

  })
});
