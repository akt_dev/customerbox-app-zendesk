/* eslint-disable no-async-promise-executor */
/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
import Zaf from './Zendesk';

const CUSTOMERBOX_URL = process.env.REACT_APP_CUSTOMERBOX_URL;
const TOKEN_API = process.env.REACT_APP_TOKEN_API;
// const url = 'http://18.214.194.1:81/api/webhook/2';
// eslint-disable-next-line max-len
// const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhY2NvdW50IjoiZWxldHJvbHV4X2N1c3RvbWVyYm94IiwiaWF0IjoxNjE3NzkxNTMxfQ.T1ZB6k3ffhoxdJe7yWlzDM0HZKktTBFx05ABXG46CUo';

const loadSettings = async () => {
  const {
    customerbox_send_message,
    customerbox_end_chat,
  } = await Zaf.getSettings();

  return {
    sendMessageAutomation: customerbox_send_message,
    endChatAutomation: customerbox_end_chat,
  };
};

const getHeaders = () => ({
  Authorization: `Bearer ${TOKEN_API}`,
});

const sendMessage = async ({
  message,
  parts,
  orderId,
  deliveryId,
}) => new Promise(async (resolve, reject) => {
  const {
    sendMessageAutomation,
  } = await loadSettings();

  const headers = getHeaders();

  const options = {
    url: `${CUSTOMERBOX_URL}/${sendMessageAutomation}`,
    type: 'POST',
    cors: false,
    contentType: 'application/json',
    headers,
    data: JSON.stringify({
      code: orderId,
      parts,
      deliveryId,
      identityId: '00007604079750',
      messageTo: 'TO_CUSTOMER',
      messageType: 'TEXT_MESSAGE',
      orderId,
      message,
    }),
  };

  Zaf.client.request(options)
    .then((data) => resolve(data))
    .catch((err) => reject(err));
});

// TODO CHANGE TO ZAF CLIENT REQUEST
const endChat = async ({ code }) => {
  const headers = getHeaders();

  const {
    endChatAutomation,
  } = await loadSettings();

  const options = {
    url: `${CUSTOMERBOX_URL}/${endChatAutomation}`,
    type: 'POST',
    cors: false,
    contentType: 'application/json',
    headers,
    data: JSON.stringify({
      code,
    }),
  };

  Zaf.client.request(options);
};

export { sendMessage, endChat };
