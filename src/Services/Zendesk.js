/* eslint-disable no-underscore-dangle */
/* eslint-disable max-len */
/* eslint-disable guard-for-in */
/* eslint-disable no-empty */
/* eslint-disable no-restricted-syntax */
import ZAFClient from 'zendesk_app_framework_sdk';

const client = ZAFClient.init();

//  const url = 'http://localhost:3000/';
const url = 'assets/index.html';

const ZAF = {
  client,
  /// Get instances | Params : location aplication , trigger
  zendeskInternalAPI: '/api/v2',
  // eslint-disable-next-line no-shadow
  makeRequests: (url, requestType, bodyParam, dataTypeParam, headersParam, secureParam, contentType, corsParam) => {
    const request = {
      url,
      type: requestType,
    };

    if (typeof secureParam !== 'undefined') {
      request.secure = secureParam;
    }

    if (typeof headersParam !== 'undefined') {
      request.headers = headersParam;
    }

    if (typeof bodyParam !== 'undefined') {
      request.data = bodyParam;
    }

    if (typeof dataTypeParam !== 'undefined') {
      request.dataType = dataTypeParam;
    }

    if (typeof corsParam !== 'undefined') {
      request.cors = corsParam;
    }

    if (typeof contentType !== 'undefined') {
      request.contentType = contentType;
    }

    return client.request(request);
  },
  getCurAssigneeInfo: () => client.get('ticket.assignee').then((data) => {
    const assignee = data['ticket.assignee'];

    return {
      group: assignee.group.name,
      user: assignee.user ? assignee.user.name : '',
      userId: assignee.user ? assignee.user.id : '',
    };
  }),
  getSettings: async () => {
    const metadata = await client.metadata();
    return metadata.settings;
  },
  getTicketInfo: async () => client.get('ticket').then((data) => data.ticket),
  getTicketFields: async () => ZAF.makeRequests('/api/v2/ticket_fields.json').then((data) => data.ticket_fields),
  getCurrentUser: async () => {
    const me = await ZAF.makeRequests('/api/v2/users/me.json');
    const { user } = me;
    const groups = await ZAF.makeRequests(`/api/v2/users/${user.id}/groups.json`);
    const group = groups.groups.length ? groups.groups[0] : { name: '' };

    return {
      group,
      user,
    };
  },
  getZendeskAPIData: (apiEndpoint, id) => client.request(`/api/v2/${apiEndpoint}/${id}.json`),
  getTicketField: (id) => client.get(`ticket.customField:custom_field_${id}`).then((tf) => tf[`ticket.customField:custom_field_${id}`]),
  hideTicketField: (id) => client.invoke(`ticketFields:custom_field_${id}.hide`),
  showTicketField: (id) => client.invoke(`ticketFields:custom_field_${id}.show`),
  disableField: (id) => client.invoke(`ticketFields:custom_field_${id}.disable`),
  hideShowTicketField: (id) => {
    client.get(`ticket.customField:custom_field_${id}`).then((data) => {
      if (data[`ticket.customField:custom_field_${id}`] === '' || data[`ticket.customField:custom_field_${id}`] === undefined) {
        client.invoke(`ticketFields:custom_field_${id}.hide`);
      } else {
        client.invoke(`ticketFields:custom_field_${id}.show`);
      }
    });
  },
  hideTicketFieldOptions: (field, options) => {
    client.get(`ticketFields:custom_field_${field}.optionValues`).then((res) => {
      const fieldOptions = res[`ticketFields:custom_field_${field}.optionValues`];
      if (typeof options === 'string') {
        const fieldOption = fieldOptions.find((opt) => opt.value === options);
        if (fieldOption) {
          const fieldOptionIndex = fieldOptions.indexOf(fieldOption);
          client.invoke(`ticketFields:custom_field_${field}.optionValues.${fieldOptionIndex}.hide`);
        }
      } else if (Array.isArray(options)) {
        fieldOptions.forEach((fieldOption, index) => {
          if (options.includes(fieldOption.value)) {
            client.invoke(`ticketFields:custom_field_${field}.optionValues.${index}.hide`);
          }
        });
      }
    });
  },
  notifyMessage: (description, type) => {
    client.invoke('notify', description, (type || undefined));
  },
  getInstances: (locationApp, trigger, obj) => client.get('instances').then((instancesData) => {
    const { instances } = instancesData;
    const appClients = [];
    for (const instanceGuid in instances) {
      if (instances[instanceGuid].location === locationApp) {
        appClients.push(client.instance(instanceGuid));
      }
    }
    return appClients;
  }).then((appClients) => {
    for (const i in appClients) {
      try {
        appClients[i].trigger(trigger, obj);
      } catch (err) {}
    }
  }),
  /// Resize | Params: width , height
  resize: (width, height) => {
    client.invoke('resize', { width, height });
  },
  modal: (modalName) => client.invoke('instances.create', {
    location: 'modal',
    url: `${url}?modal=${modalName}`,
  }).then((modalContext) => modalContext['instances.create'][0].instanceGuid),
  modalListItens: (modalName, context) => {
    const parentGuid = context._instanceGuid;
    const options = {
      location: 'modal',
      url: `${url}?modal=${modalName}#parent_guid=${parentGuid}`,
    };
    client.invoke('instances.create', options);
  },
  addEventOnChanged: (fieldID, callback) => {
    client.on(`ticket.custom_field_${fieldID}.changed`, callback);
  },
  // eslint-disable-next-line consistent-return
  isPageModal: (search) => {
    if (search) return search.indexOf('modal') >= 0;
  },
  updateTicket: async (ticketId, data) => {
    const headers = { 'Content-Type': 'application/json' };
    return ZAF.makeRequests(`/api/v2/tickets/${ticketId}.json`, 'PUT', JSON.stringify(data), 'json', headers);
  },
  getCustomField: async (customFieldId) => ZAF.client.on(`ticket.customField:custom_field_${customFieldId}`),
  getComments: async (ticketId) => {
    const headers = { 'Content-Type': 'application/json' };
    return ZAF.makeRequests(`/api/v2/tickets/${ticketId}/comments`, 'GET', headers);
  },
  updateCustomField: async ({ ticketId, customFieldId, customFieldValue }) => {
    const headers = { 'Content-Type': 'application/json' };
    const body = {
      ticket: {
        custom_fields: [{
          id: customFieldId,
          value: customFieldValue,
        }],
      },
    };

    return ZAF.makeRequests(`/api/v2/tickets/${ticketId}.json`, 'PUT', JSON.stringify(body), 'json', headers);
  },
  makeCommentPrivate: async ({ ticketId, commentId }) => {
    const headers = { 'Content-Type': 'application/json' };
    try {
      return ZAF.makeRequests(`/api/v2/tickets/${ticketId}/comments/${commentId}/make_private`, 'PUT', headers);
    } catch (err) {
      return err;
    }
  },
};

export default ZAF;
